from django.contrib import admin
from django.urls import path
import todo.views
import vocab.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', todo.views.api_index, name='api_index'),
    path('todo/all/', todo.views.api_all_tasks, name='api_all_tasks'),
    path('todo/new/', todo.views.api_new_task, name='api_new_task'),
    path('todo/update/', todo.views.api_update_task, name='api_update_task'),
    path('todo/delete/', todo.views.api_delete_task, name='api_delete_task'),
    path('todo/signin/', todo.views.api_signin, name='api_signin'),

    path('api/signin/', vocab.views.api_signin, name='api_signin'),
    path('api/create_collection/', vocab.views.api_create_collection, name='api_create_collection'),
    path('api/get_collections/', vocab.views.api_get_collections, name='api_get_collections'),
    path('api/update_collection/', vocab.views.api_update_collection, name='api_update_collection'),
    path('api/delete_collection/', vocab.views.api_delete_collection, name='api_delete_collection'),
    path('api/parse_text/', vocab.views.api_parse_text, name='api_parse_text')
]
