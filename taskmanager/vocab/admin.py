from django.contrib import admin
from .models import Word, Collection

# Register your models here.
class WordAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Word._meta.fields]

class CollectionAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Collection._meta.fields]

admin.site.register(Word, WordAdmin)
admin.site.register(Collection, CollectionAdmin)