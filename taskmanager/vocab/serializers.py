from rest_framework import serializers
from .models import Word, Collection


class wordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = (
            'id',
            'token',
            'definition',
        )

class collectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = (
            'id',
            'user',
            'last_change',
        )
