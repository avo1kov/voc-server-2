from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.status import HTTP_400_BAD_REQUEST
# More rest imports as needed
from django.contrib.auth import authenticate
import datetime
from datetime import datetime, date, timedelta
from .decorators import define_usage
from .models import Word, Collection
from .serializers import wordSerializer, collectionSerializer
import logging
import json

import gensim
from gensim.models import word2vec
import nltk
from nltk.stem import WordNetLemmatizer 
import numpy as np
import gensim.downloader as api
from sklearn.metrics import silhouette_score
from sklearn.cluster import KMeans
from gensim.models import KeyedVectors
from threading import Semaphore

model_wiki = gensim.models.KeyedVectors.load_word2vec_format('/Users/avo1kov/gensim-data/glove-wiki-gigaword-50/glove-wiki-gigaword-50.gz')

logger = logging.getLogger('app_api')

# Create your views here.

#URL /signin/
#Note that in a real Django project, signin and signup would most likely be
#handled by a seperate app. For signup on this example, use the admin panel.
@define_usage(params={'username': 'String', 'password': 'String'},
              returns={'authenticated': 'Bool', 'token': 'Token String'})
@api_view(['POST'])
@permission_classes((AllowAny,))
def api_signin(request):
    try:
        username = request.data['username']
        password = request.data['password']
    except:
        return Response({'error': 'Please provide correct username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if user is not None:
        token, _ = Token.objects.get_or_create(user=user)
        return Response({'authenticated': True, 'token': "Token " + token.key})
    else:
        return Response({'authenticated': False, 'token': None})

#URL /create_collection/
@define_usage(params={'emoji': 'String', 'nane': 'String', 'words': 'Dict'},
              returns={'success': 'Bool'})
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def api_create_collection(request):
  try:
    emoji = request.data['emoji']
    collection_name = request.data['name']
    words = request.data['words']
    collection_sample = Collection(emoji = emoji, name = collection_name, user = request.user, last_change = datetime.now())
    collection_sample.save()
    words_string = ""
    word_ids = []
    for word in words:
      try:
        existed_word = Word.objects.get(token=word['token'], definition=word['definition'])
        collection_sample.words.add(existed_word)
        word_ids.append(existed_word.id)
      except Exception as e:
        word_sample = Word(token = word['token'], definition = word['definition'])
        word_sample.save()
        collection_sample.words.add(word_sample)
        word_ids.append(word_sample.id)
      words_string += word['token']
    collection_sample.words.add(*word_ids)
    return Response({'success': True})
  except Exception as e:
    logger.error(e)
  return Response({'success': False})

#URL /get_collection/
@define_usage(returns={'collections': 'Dict'})
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def api_get_collections(request):
  collections = request.user.collection_set.all()
  response = []
  for collection in collections:
    words_instance = collection.words.all()
    words = []
    for word in words_instance:
      words.append({
        'token': word.token,
        'definition': word.definition
      })
    response.append({
      "id": collection.id,
      "emoji": collection.emoji,
      "name": collection.name,
      "last_change": str(collection.last_change.strftime('%s')),
      "words": words
    })
  return Response({'collections': response})

#URL /update_collection/
@define_usage(params={'id': 'Integer', 'emoji': 'String', 'name': 'String', 'words': 'Dict'},
              returns={'success': 'Bool'})
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def api_update_collection(request):
  collection = request.user.collection_set.get(id=int(request.data['id']))
  try:
    collection.emoji = request.data['emoji']
  except:
    pass
  try:
    collection.name = request.data['name']
  except:
    pass
  try:
    new_words = request.data['words']
    words_instances = collection.words.all()
    for word_instance in words_instances:
      logger.error(word_instance.token)
      needful_token = False
      for new_word in new_words:
        if word_instance.token == new_word['token']:
          word_instance.definition = new_word['definition']
          needful_token = True
      if not needful_token:
        collection.words.remove(word_instance)
    for new_word in new_words:
      word_for_add = True
      for word_instance in words_instances:
        if new_word['token'] == word_instance.token:
          word_for_add = False
      if word_for_add:
        try:
          existed_word = Word.objects.get(token=new_word['token'], definition=new_word['definition'])
          collection.words.add(existed_word)
        except Exception as e:
          new_word_sample = Word(token=new_word['token'], definition=new_word['definition'])
          new_word_sample.save()
          collection.words.add(new_word_sample)
  except:
    pass
  # collection.last_change = date.now()
  collection.save()
  return Response({'success': True})

#URL /delete_collection/
@define_usage(params={'id': 'Int'},
              returns={'success': 'Bool'})
@api_view(['DELETE'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def api_delete_collection(request):
    collection = request.user.collection_set.get(id=int(request.data['id']))
    collection.delete()
    return Response({'success': True})

def get_groups(text):
  tokenized_corpus = nltk.word_tokenize(text.decode().lower())
  poses = nltk.pos_tag(tokenized_corpus)

  tokens = set()
  lemmatizer = WordNetLemmatizer()
  for t in poses:
      if t[1] in ['JJ', 'FW', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS', 'PDT', 'RP', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
          pos = t[1][0].lower() if t[1][0].lower() != 'j' else 'a'
          lemma = lemmatizer.lemmatize(t[0], pos)
          tokens.update([lemma])

  X = []
  words = []
  for t in tokens:
      try:
          X.append(model_wiki[t])
          words.append(t)
      except:
          pass
  try:
    sil = []
    kmax = 10
    for k in range(2, kmax+1):
        kmeans = KMeans(n_clusters = k).fit(X)
        labels = kmeans.labels_
        sil.append(silhouette_score(X, labels, metric = 'euclidean'))
  except Exception as e:
    print(e)
  #
  NUM_CLUSTERS = sil.index(max(sil)) + 2
  k_means = KMeans(n_clusters = NUM_CLUSTERS, n_init = 12)
  k_means.fit(X)
  labels = k_means.labels_

  clusters = [[] for i in range(NUM_CLUSTERS)]
  sums = [np.zeros(X[0].shape) for i in range(NUM_CLUSTERS)]
  for i, word in enumerate(words):
      sums[labels[i]] += X[i]
      clusters[labels[i]].append(word)

  return clusters

@api_view(['POST'])
def api_parse_text(request):
  try:
    if request.method == 'POST':
        # form = UploadFileForm(request.POST, request.FILES)
        # if form.is_valid():
        text = request.FILES.get('file').read()
        return Response({'success': True, 'groups': get_groups(text)})
  except:
    pass
  return Response({'success': False})
