from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Word(models.Model):
  token = models.CharField(max_length=150)
  definition = models.CharField(max_length=150)

  def __str__(self):
        return self.token

class Collection(models.Model):
  emoji = models.CharField(max_length=10, blank=True)
  name = models.CharField(max_length=150, blank=True)
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  last_change = models.DateTimeField()
  words = models.ManyToManyField(Word)

  def __str__(self):
        return self.name